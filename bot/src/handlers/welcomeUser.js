const { Markup } = require('telegraf');

module.exports = (ctx) => {
  ctx.replyWithPhoto({
    source: 'src/assets/logo.png',
  });
  ctx.reply(
    'Добро пожаловать в бота по выдаче акков и карт от 21GOLD!',
    Markup.keyboard(['Получить аккаунты 👥', 'Получить карты 💳']),
  );
};
