const { Scenes } = require('telegraf');

const cardScene = new Scenes.BaseScene('card');

cardScene.enter((ctx) => {
  ctx.reply('Hello');
});

cardScene.leave((ctx) => {
  ctx.reply('Bye');
});

module.exports = cardScene;
