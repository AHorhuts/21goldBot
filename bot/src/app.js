require('dotenv').config();
const { Telegraf, Scenes: { Stage } } = require('telegraf');
const LocalSession = require('telegraf-session-local');
const scenes = require('./scenes/index.js');
const welcomeUser = require('./handlers/welcomeUser.js');
const clearSession = require('./handlers/clearSession.js');

const scaffold = (config = {}) => {
  const bot = new Telegraf(process.env.BOT_TOKEN, config);

  bot.telegram.setMyCommands([
    {
      command: 'start',
      description: 'Запустить бота',
    },
    {
      command: 'newcards',
      description: 'Получить карты 💳',
    },
    {
      command: 'newaccounts',
      description: 'Получить аккаунты 👥',
    },
    {
      command: 'clearsession',
      description: 'Очистить текущую сессию (некритичные данные)',
    },
    {
      command: 'cancel',
      description: 'Отменить текущую операцию',
    },
    {
      command: 'help',
      description: 'Получить справку бота',
    },
  ]);

  bot.use(new LocalSession({ storage: LocalSession.storageFileAsync }).middleware());
  bot.use(new Stage(scenes).middleware());

  bot.command('start', welcomeUser);
  bot.command('clearsession', clearSession);

  bot.command('newcards', (ctx) => ctx.scene.enter('card'));
  bot.action('getCards', (ctx) => ctx.scene.enter('card'));

  return bot;
};

module.exports = scaffold;
