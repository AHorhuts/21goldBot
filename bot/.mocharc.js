module.exports = {
  color: true,
  exit: true,
  require: 'dotenv/config',
  spec: ['./**/*.spec.js'],
};
