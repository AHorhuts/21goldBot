/* eslint-disable no-undef */
const TelegramServer = require('telegram-test-api');
const scaffold = require('./src/app.js');

describe('Telegram bot test', () => {
  const server = new TelegramServer();
  let client;

  before(async () => {
    await server.start();
    client = server.getClient(process.env.BOT_TOKEN);
    const bot = scaffold({
      telegram: {
        apiRoot: 'http://localhost:9000',
      },
    });
    bot.launch();
  });

  after(() => server.stop());

  it('Should return help content', async () => {
    await client.sendCommand(client.makeCommand('/start'));
    const updates = await client.getUpdates();
    return updates.ok;
  });
});
