const path = require('node:path');
const fastify = require('fastify');
const autoload = require('@fastify/autoload');

const scaffold = (config = {}) => {
  const app = fastify(config);

  app.register(autoload, {
    dir: path.join(__dirname, 'routes'),
  });

  return app;
};

module.exports = scaffold;
