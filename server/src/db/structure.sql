CREATE TABLE accounts (
	id serial NOT NULL,
	"type" text,
	CONSTRAINT pk_accounts PRIMARY KEY (id)
);

CREATE TABLE cards (
	id serial NOT NULL,
	"type" text,
	CONSTRAINT pk_cards PRIMARY KEY (id)
);

CREATE TABLE users (
	id serial NOT NULL,
	name text,
	CONSTRAINT pk_users PRIMARY KEY (id)
);

CREATE TABLE users_accounts (
	user_id integer NOT NULL,
	account_id integer NOT NULL,
	CONSTRAINT unq_users_accounts_account_id UNIQUE (account_id)
);

CREATE TABLE users_cards (
	user_id integer NOT NULL,
	card_id integer NOT NULL,
	CONSTRAINT unq_users_cards_card_id UNIQUE (card_id)
);

ALTER TABLE users_accounts ADD CONSTRAINT fk_users_accounts_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;

ALTER TABLE users_accounts ADD CONSTRAINT fk_users_accounts_accounts FOREIGN KEY (account_id) REFERENCES accounts(id) ON DELETE CASCADE;

ALTER TABLE users_cards ADD CONSTRAINT fk_users_cards_cards FOREIGN KEY (card_id) REFERENCES cards(id) ON DELETE CASCADE;

ALTER TABLE users_cards ADD CONSTRAINT fk_users_cards_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;
