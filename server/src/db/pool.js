const { Pool } = require('pg');

const scaffoldDatabase = (console) => {
  console.info('Connection to the database');

  const pool = new Pool({
    host: process.env.POSTGRES_HOST,
    port: 5432,
    database: process.env.POSTGRES_DB,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
  });

  return pool;
};

module.exports = scaffoldDatabase;
