#!/bin/bash

PSQL="$PWD/src/db/psql.sh"

if [ "$1" = "migrate" ]; then
    $PSQL "$PWD/src/db/structure.sql"
elif [ "$1" = "seed" ]; then
    $PSQL "$PWD/src/db/data.sql"
fi
