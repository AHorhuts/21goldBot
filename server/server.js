require('dotenv').config();
const scaffold = require('./src/app.js');

const app = scaffold({ logger: true });

try {
  app.listen({ port: 4000 });
} catch (error) {
  app.log.error(error);
  process.exit(1);
}
