## Environment variables

```shell
POSTGRES_HOST="yourPostgresHostAddress" # for example "localhost"
POSTGRES_DB="yourPostgresDatabaseName" # for example "bot"
POSTGRES_USER="yourPostgresUserName" # for example "bot"
POSTGRES_PASSWORD="yourPostgresUserPassword" # for example "bot"
```
