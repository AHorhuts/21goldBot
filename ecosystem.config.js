require('dotenv').config();

module.exports = {
  apps: [
    {
      name: '21goldBot',
      cwd: 'bot',
      script: 'bot.js',
      watch: ['bot.js', 'src'],
      ignore_watch: ['src/assets', './**/*.spec.js'],
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
    {
      name: '21goldBotServer',
      cwd: 'server',
      script: 'server.js',
      watch: ['server.js', 'src'],
      ignore_watch: ['src/db/*.{sh,sql}'],
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],
  deploy: {
    production: {
      key: process.env.SSH_KEY_FILE_PATH,
      user: 'horhuts',
      host: '31.220.56.59',
      ref: 'origin/main',
      repo: 'https://gitlab.com/AHorhuts/21goldBot.git',
      path: '/var/www/horhuts/data/21goldBot',
      'post-setup': 'npm install --omit=dev --workspaces',
      'pre-deploy-local': 'npm run lint --workspaces',
      'post-deploy': 'npm install --omit=dev --workspaces',
    },
  },
};
